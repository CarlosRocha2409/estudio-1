﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Estudio_2
{
    
    
    public partial class GestionExtintores : Form   
    {
        DataSet dsExtintor;
        BindingSource bsExtintor;

        public GestionExtintores()
        {
            InitializeComponent();
            bsExtintor = new BindingSource();
        }

        public DataSet DsExtintor { get => dsExtintor; set => dsExtintor = value; }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            FrmExtintor fe = new FrmExtintor();
            fe.DsExtintor = DsExtintor;
            fe.TblExtintor = DsExtintor.Tables["Extintor"];
            fe.ShowDialog();

        }

        private void TxtFinder_TextChanged(object sender, EventArgs e)

        {
            try
            {
                bsExtintor.Filter = string.Format("Id like '*{0}*' or Tipo like '*{0}*', or Medida like '*{0}*',or Marca like '*{0}*' ", txtFinder.Text);
            }catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void Editar_Click(object sender, EventArgs e)
            
        {
            DataGridViewSelectedRowCollection rowCollection = dataGridView1.SelectedRows;
            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder editar", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }

            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;


            FrmExtintor fe = new FrmExtintor();
            fe.DsExtintor = DsExtintor;
            fe.TblExtintor = DsExtintor.Tables["Extintor"];
            fe.DrExtintor = drow;
            fe.ShowDialog();
            
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dataGridView1.SelectedRows;
            if (rowCollection.Count == 0)

            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder editar", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

                

            }
            DataGridViewRow gridrow = rowCollection[0];
            DataRow drow = ((DataRowView)gridrow.DataBoundItem).Row;

            DialogResult result= MessageBox.Show(this, "Realmente Quiere Eliminar Esta Column?", "Mensaje De Error", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            
            if (result == DialogResult.Yes)
            {
                DsExtintor.Tables["Extintor"].Rows.Remove(drow);

            }
        }

        private void GestionExtintores_Load(object sender, EventArgs e)
        {
            bsExtintor.DataSource = DsExtintor;
            bsExtintor.DataMember = DsExtintor.Tables["Extintor"].TableName;
            dataGridView1.DataSource = bsExtintor;
            dataGridView1.AutoGenerateColumns = true;
        }
    }
}
