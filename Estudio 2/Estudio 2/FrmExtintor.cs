﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Estudio_2
{
    public partial class FrmExtintor : Form
    {
        DataSet dsExtintor;
        BindingSource bsExtintor;
        DataTable tblExtintor;
        DataRow drExtintor;
        public FrmExtintor()
        {
            InitializeComponent();
            bsExtintor = new BindingSource();
        }

        public DataSet DsExtintor { get => dsExtintor; set => dsExtintor = value; }
        public DataTable TblExtintor { get => tblExtintor; set => tblExtintor = value; }
        public DataRow DrExtintor {
            set {
                drExtintor = value;
                cmbTipo.SelectedItem = drExtintor["Tipo"].ToString();
                cmbMarca.SelectedItem = drExtintor["Marca"].ToString();
                cmbMedida.SelectedItem = drExtintor["Medida"].ToString();
                cmbCapacidad.SelectedItem = drExtintor["Capacidad"].ToString();
                txtLugar.Text = drExtintor["Lugar de Colocacion"].ToString();
                txtPrecio.Text = drExtintor["Precio"].ToString();
                
                }
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            String Tipo, Marca, Medida, Capacidad, Lugar;
            Double Precio;
            Tipo = cmbTipo.SelectedItem.ToString();
            Marca = cmbMarca.SelectedItem.ToString();
            Medida = cmbMedida.SelectedItem.ToString();
            Capacidad = cmbCapacidad.SelectedItem.ToString();
            Lugar = txtLugar.Text;
            Precio = Double.Parse(txtPrecio.Text);

            DateTime fecha = DateTime.Now;

            if(drExtintor!= null)
            {
                DataRow drnew = tblExtintor.NewRow();
                int index = tblExtintor.Rows.IndexOf(drExtintor);
                drnew["Id"] = drExtintor["Id"];
                drnew["Tipo"] = Tipo;
                drnew["Marca"] = Marca;
                drnew["Medida"] = Medida;
                drnew["Capacidad"] = Capacidad;
                drnew["Lugar de Colocacion"] = Lugar;
                drnew["Precio"] = Precio;

                TblExtintor.Rows.RemoveAt(index);
                TblExtintor.Rows.InsertAt(drnew, index);
                TblExtintor.Rows[index].AcceptChanges();
                TblExtintor.Rows[index].SetModified();
            }
            else
            {
                TblExtintor.Rows.Add(TblExtintor.Rows.Count + 1, Tipo, Marca, Medida, Capacidad, Lugar, fecha,Tipo+" "+Marca,Precio);
            }

        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
