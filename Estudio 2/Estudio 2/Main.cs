﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Estudio_2
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void ExtintoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GestionExtintores ge = new GestionExtintores();
            ge.DsExtintor = dsExtintores;
            ge.Show();

        }

        private void ClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GestionCliente gc = new GestionCliente();
            gc.DsCliente = dsExtintores;
            gc.Show();
        }

        private void NuevaFacturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmFactura ff = new FrmFactura();
            ff.DsFactura = dsExtintores;
            ff.Show();
        }
    }
}
