﻿namespace Estudio_2
{
    partial class Main
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.inicioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.extintoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.facturarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevaFacturaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dsExtintores = new System.Data.DataSet();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn16 = new System.Data.DataColumn();
            this.dataColumn23 = new System.Data.DataColumn();
            this.dataTable2 = new System.Data.DataTable();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataColumn13 = new System.Data.DataColumn();
            this.dataColumn14 = new System.Data.DataColumn();
            this.dataColumn15 = new System.Data.DataColumn();
            this.dataColumn17 = new System.Data.DataColumn();
            this.dataTable3 = new System.Data.DataTable();
            this.dataColumn18 = new System.Data.DataColumn();
            this.dataColumn19 = new System.Data.DataColumn();
            this.dataColumn20 = new System.Data.DataColumn();
            this.dataColumn21 = new System.Data.DataColumn();
            this.dataColumn22 = new System.Data.DataColumn();
            this.dataTable4 = new System.Data.DataTable();
            this.dataColumn24 = new System.Data.DataColumn();
            this.dataColumn25 = new System.Data.DataColumn();
            this.dataColumn26 = new System.Data.DataColumn();
            this.dataColumn27 = new System.Data.DataColumn();
            this.dataColumn28 = new System.Data.DataColumn();
            this.dataColumn29 = new System.Data.DataColumn();
            this.dataColumn34 = new System.Data.DataColumn();
            this.dataTable5 = new System.Data.DataTable();
            this.dataColumn30 = new System.Data.DataColumn();
            this.dataColumn31 = new System.Data.DataColumn();
            this.dataColumn32 = new System.Data.DataColumn();
            this.dataColumn33 = new System.Data.DataColumn();
            this.bsExtintores = new System.Windows.Forms.BindingSource(this.components);
            this.dataTable6 = new System.Data.DataTable();
            this.dataColumn35 = new System.Data.DataColumn();
            this.dataColumn36 = new System.Data.DataColumn();
            this.dataColumn37 = new System.Data.DataColumn();
            this.dataColumn38 = new System.Data.DataColumn();
            this.dataColumn39 = new System.Data.DataColumn();
            this.dataColumn40 = new System.Data.DataColumn();
            this.dataColumn41 = new System.Data.DataColumn();
            this.dataColumn42 = new System.Data.DataColumn();
            this.dataColumn43 = new System.Data.DataColumn();
            this.dataColumn44 = new System.Data.DataColumn();
            this.dataColumn45 = new System.Data.DataColumn();
            this.dataColumn46 = new System.Data.DataColumn();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsExtintores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsExtintores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable6)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inicioToolStripMenuItem,
            this.facturarToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // inicioToolStripMenuItem
            // 
            this.inicioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.extintoresToolStripMenuItem,
            this.clientesToolStripMenuItem});
            this.inicioToolStripMenuItem.Name = "inicioToolStripMenuItem";
            this.inicioToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.inicioToolStripMenuItem.Text = "inicio";
            // 
            // extintoresToolStripMenuItem
            // 
            this.extintoresToolStripMenuItem.Name = "extintoresToolStripMenuItem";
            this.extintoresToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.extintoresToolStripMenuItem.Text = "Extintores";
            this.extintoresToolStripMenuItem.Click += new System.EventHandler(this.ExtintoresToolStripMenuItem_Click);
            // 
            // clientesToolStripMenuItem
            // 
            this.clientesToolStripMenuItem.Name = "clientesToolStripMenuItem";
            this.clientesToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.clientesToolStripMenuItem.Text = "Clientes";
            this.clientesToolStripMenuItem.Click += new System.EventHandler(this.ClientesToolStripMenuItem_Click);
            // 
            // facturarToolStripMenuItem
            // 
            this.facturarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevaFacturaToolStripMenuItem});
            this.facturarToolStripMenuItem.Name = "facturarToolStripMenuItem";
            this.facturarToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.facturarToolStripMenuItem.Text = "Facturar";
            // 
            // nuevaFacturaToolStripMenuItem
            // 
            this.nuevaFacturaToolStripMenuItem.Name = "nuevaFacturaToolStripMenuItem";
            this.nuevaFacturaToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.nuevaFacturaToolStripMenuItem.Text = "Nueva Factura";
            this.nuevaFacturaToolStripMenuItem.Click += new System.EventHandler(this.NuevaFacturaToolStripMenuItem_Click);
            // 
            // dsExtintores
            // 
            this.dsExtintores.DataSetName = "NewDataSet";
            this.dsExtintores.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1,
            this.dataTable2,
            this.dataTable3,
            this.dataTable4,
            this.dataTable5,
            this.dataTable6});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn16,
            this.dataColumn23});
            this.dataTable1.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "id"}, true)});
            this.dataTable1.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn1};
            this.dataTable1.TableName = "Extintor";
            // 
            // dataColumn1
            // 
            this.dataColumn1.AllowDBNull = false;
            this.dataColumn1.ColumnName = "id";
            this.dataColumn1.DataType = typeof(int);
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "Tipo";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "Marca";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "Medida";
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "Capacidad";
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "Lugar De Colocacion";
            // 
            // dataColumn7
            // 
            this.dataColumn7.ColumnName = "Fecha";
            this.dataColumn7.DataType = typeof(System.DateTime);
            // 
            // dataColumn16
            // 
            this.dataColumn16.ColumnMapping = System.Data.MappingType.Hidden;
            this.dataColumn16.ColumnName = "TM";
            // 
            // dataColumn23
            // 
            this.dataColumn23.ColumnName = "Precio";
            this.dataColumn23.DataType = typeof(double);
            // 
            // dataTable2
            // 
            this.dataTable2.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn8,
            this.dataColumn9,
            this.dataColumn10,
            this.dataColumn11,
            this.dataColumn12,
            this.dataColumn13,
            this.dataColumn14,
            this.dataColumn15,
            this.dataColumn17});
            this.dataTable2.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
            this.dataTable2.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn8};
            this.dataTable2.TableName = "Cliente";
            // 
            // dataColumn8
            // 
            this.dataColumn8.AllowDBNull = false;
            this.dataColumn8.ColumnName = "Id";
            this.dataColumn8.DataType = typeof(int);
            // 
            // dataColumn9
            // 
            this.dataColumn9.ColumnName = "Cedula";
            // 
            // dataColumn10
            // 
            this.dataColumn10.ColumnName = "Nombre";
            // 
            // dataColumn11
            // 
            this.dataColumn11.ColumnName = "Apellido";
            // 
            // dataColumn12
            // 
            this.dataColumn12.ColumnName = "Celular";
            // 
            // dataColumn13
            // 
            this.dataColumn13.ColumnName = "Direccion";
            // 
            // dataColumn14
            // 
            this.dataColumn14.ColumnName = "Municipio";
            // 
            // dataColumn15
            // 
            this.dataColumn15.ColumnName = "Departamento";
            // 
            // dataColumn17
            // 
            this.dataColumn17.ColumnMapping = System.Data.MappingType.Hidden;
            this.dataColumn17.ColumnName = "NA";
            // 
            // dataTable3
            // 
            this.dataTable3.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn18,
            this.dataColumn19,
            this.dataColumn20,
            this.dataColumn21,
            this.dataColumn22});
            this.dataTable3.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
            this.dataTable3.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn18};
            this.dataTable3.TableName = "ExtintorFactura";
            // 
            // dataColumn18
            // 
            this.dataColumn18.AllowDBNull = false;
            this.dataColumn18.ColumnName = "Id";
            this.dataColumn18.DataType = typeof(int);
            // 
            // dataColumn19
            // 
            this.dataColumn19.ColumnName = "Tipo";
            // 
            // dataColumn20
            // 
            this.dataColumn20.ColumnName = "Marca";
            // 
            // dataColumn21
            // 
            this.dataColumn21.ColumnName = "Cantidad";
            this.dataColumn21.DataType = typeof(int);
            // 
            // dataColumn22
            // 
            this.dataColumn22.ColumnName = "Precio";
            this.dataColumn22.DataType = typeof(decimal);
            // 
            // dataTable4
            // 
            this.dataTable4.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn24,
            this.dataColumn25,
            this.dataColumn26,
            this.dataColumn27,
            this.dataColumn28,
            this.dataColumn29,
            this.dataColumn34});
            this.dataTable4.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
            this.dataTable4.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn24};
            this.dataTable4.TableName = "Factura";
            // 
            // dataColumn24
            // 
            this.dataColumn24.AllowDBNull = false;
            this.dataColumn24.ColumnName = "Id";
            this.dataColumn24.DataType = typeof(int);
            // 
            // dataColumn25
            // 
            this.dataColumn25.ColumnName = "Extintor";
            // 
            // dataColumn26
            // 
            this.dataColumn26.ColumnName = "Cliente";
            // 
            // dataColumn27
            // 
            this.dataColumn27.ColumnName = "Subtotal";
            this.dataColumn27.DataType = typeof(double);
            // 
            // dataColumn28
            // 
            this.dataColumn28.ColumnName = "Iva";
            this.dataColumn28.DataType = typeof(double);
            // 
            // dataColumn29
            // 
            this.dataColumn29.ColumnName = "Total";
            this.dataColumn29.DataType = typeof(double);
            // 
            // dataColumn34
            // 
            this.dataColumn34.ColumnName = "Fecha";
            this.dataColumn34.DataType = typeof(System.DateTime);
            // 
            // dataTable5
            // 
            this.dataTable5.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn30,
            this.dataColumn31,
            this.dataColumn32,
            this.dataColumn33});
            this.dataTable5.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
            this.dataTable5.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn30};
            this.dataTable5.TableName = "DetalleFactura";
            // 
            // dataColumn30
            // 
            this.dataColumn30.AllowDBNull = false;
            this.dataColumn30.ColumnName = "Id";
            this.dataColumn30.DataType = typeof(int);
            // 
            // dataColumn31
            // 
            this.dataColumn31.ColumnName = "Extintor";
            // 
            // dataColumn32
            // 
            this.dataColumn32.ColumnName = "Cantidad";
            this.dataColumn32.DataType = typeof(int);
            // 
            // dataColumn33
            // 
            this.dataColumn33.ColumnName = "Precio";
            this.dataColumn33.DataType = typeof(double);
            // 
            // dataTable6
            // 
            this.dataTable6.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn35,
            this.dataColumn36,
            this.dataColumn37,
            this.dataColumn38,
            this.dataColumn39,
            this.dataColumn40,
            this.dataColumn41,
            this.dataColumn42,
            this.dataColumn43,
            this.dataColumn44,
            this.dataColumn45,
            this.dataColumn46});
            this.dataTable6.TableName = "ReporteFactura";
            // 
            // dataColumn35
            // 
            this.dataColumn35.ColumnName = "Id_Factura";
            this.dataColumn35.DataType = typeof(int);
            // 
            // dataColumn36
            // 
            this.dataColumn36.ColumnName = "Id_Cliente";
            this.dataColumn36.DataType = typeof(int);
            // 
            // dataColumn37
            // 
            this.dataColumn37.ColumnName = "Subtotal";
            this.dataColumn37.DataType = typeof(double);
            // 
            // dataColumn38
            // 
            this.dataColumn38.ColumnName = "Iva";
            this.dataColumn38.DataType = typeof(int);
            // 
            // dataColumn39
            // 
            this.dataColumn39.ColumnName = "Total";
            this.dataColumn39.DataType = typeof(int);
            // 
            // dataColumn40
            // 
            this.dataColumn40.ColumnName = "Id_Codigo";
            this.dataColumn40.DataType = typeof(int);
            // 
            // dataColumn41
            // 
            this.dataColumn41.ColumnName = "Id_Producto";
            this.dataColumn41.DataType = typeof(int);
            // 
            // dataColumn42
            // 
            this.dataColumn42.ColumnName = "Cantidad";
            this.dataColumn42.DataType = typeof(int);
            // 
            // dataColumn43
            // 
            this.dataColumn43.ColumnName = "Precio";
            this.dataColumn43.DataType = typeof(double);
            // 
            // dataColumn44
            // 
            this.dataColumn44.ColumnName = "Nombre_Producto";
            // 
            // dataColumn45
            // 
            this.dataColumn45.ColumnName = "Nombre_Cliente";
            // 
            // dataColumn46
            // 
            this.dataColumn46.ColumnName = "Apellido_Cliente";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Main";
            this.Text = "Extintores";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsExtintores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsExtintores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable6)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem inicioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem extintoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem facturarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevaFacturaToolStripMenuItem;
        private System.Data.DataSet dsExtintores;
        private System.Windows.Forms.BindingSource bsExtintores;
        private System.Data.DataTable dataTable1;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Data.DataColumn dataColumn7;
        private System.Data.DataTable dataTable2;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataColumn dataColumn9;
        private System.Data.DataColumn dataColumn10;
        private System.Data.DataColumn dataColumn11;
        private System.Data.DataColumn dataColumn12;
        private System.Data.DataColumn dataColumn13;
        private System.Data.DataColumn dataColumn14;
        private System.Data.DataColumn dataColumn15;
        private System.Data.DataColumn dataColumn16;
        private System.Data.DataColumn dataColumn17;
        private System.Data.DataTable dataTable3;
        private System.Data.DataColumn dataColumn18;
        private System.Data.DataColumn dataColumn19;
        private System.Data.DataColumn dataColumn20;
        private System.Data.DataColumn dataColumn21;
        private System.Data.DataColumn dataColumn22;
        private System.Data.DataColumn dataColumn23;
        private System.Data.DataTable dataTable4;
        private System.Data.DataColumn dataColumn24;
        private System.Data.DataTable dataTable5;
        private System.Data.DataColumn dataColumn30;
        private System.Data.DataColumn dataColumn31;
        private System.Data.DataColumn dataColumn32;
        private System.Data.DataColumn dataColumn33;
        private System.Data.DataColumn dataColumn25;
        private System.Data.DataColumn dataColumn26;
        private System.Data.DataColumn dataColumn27;
        private System.Data.DataColumn dataColumn28;
        private System.Data.DataColumn dataColumn29;
        private System.Data.DataColumn dataColumn34;
        private System.Data.DataTable dataTable6;
        private System.Data.DataColumn dataColumn35;
        private System.Data.DataColumn dataColumn36;
        private System.Data.DataColumn dataColumn37;
        private System.Data.DataColumn dataColumn38;
        private System.Data.DataColumn dataColumn39;
        private System.Data.DataColumn dataColumn40;
        private System.Data.DataColumn dataColumn41;
        private System.Data.DataColumn dataColumn42;
        private System.Data.DataColumn dataColumn43;
        private System.Data.DataColumn dataColumn44;
        private System.Data.DataColumn dataColumn45;
        private System.Data.DataColumn dataColumn46;
    }
}

