﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estudio_2.Entities
{
    class Extintor
    {

        int id;
        String tipo, marca, medida, capacidad, lugarDeColocacion;
        DateTime fecha;

        public Extintor(int id, string tipo, string marca, string medida, string capacidad, string lugarDeColocacion,DateTime fecha)
        {
            this.id = id;
            this.tipo = tipo;
            this.marca = marca;
            this.medida = medida;
            this.capacidad = capacidad;
            this.lugarDeColocacion = lugarDeColocacion;
            this.fecha = fecha;
        }

        public int Id { get => id; set => id = value; }
        public string Tipo { get => tipo; set => tipo = value; }
        public string Marca { get => marca; set => marca = value; }
        public string Medida { get => medida; set => medida = value; }
        public string Capacidad { get => capacidad; set => capacidad = value; }
        public string LugarDeColocacion { get => lugarDeColocacion; set => lugarDeColocacion = value; }
        public DateTime Fecha { get => fecha; set => fecha = value; }
    }
}
