﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estudio_2.Entities
{
    class ReporteFactura
    {

        //Factura
        private int id_Factura;
        private int id_cliente;
        private double subtotal, iva, total;

        //detallefactura

        private int id_codigo;
        private int id_producto;
        private int cantida;
        private double Precio;

        //producto
        private string nombre_producto;

        //cliente
        private string nombre_cliente;
        private string apellido_cliente;

        public ReporteFactura(int id_Factura, int id_cliente, double subtotal, double iva, double total, int id_codigo, int id_producto, int cantida, double precio, string nombre_producto, string nombre_cliente, string apellido_cliente)
        {
            this.id_Factura = id_Factura;
            this.id_cliente = id_cliente;
            this.subtotal = subtotal;
            this.iva = iva;
            this.total = total;
            this.id_codigo = id_codigo;
            this.id_producto = id_producto;
            this.cantida = cantida;
            Precio = precio;
            this.nombre_producto = nombre_producto;
            this.nombre_cliente = nombre_cliente;
            this.apellido_cliente = apellido_cliente;
        }

        public int Id_Factura { get => id_Factura; set => id_Factura = value; }
        public int Id_cliente { get => id_cliente; set => id_cliente = value; }
        public double Subtotal { get => subtotal; set => subtotal = value; }
        public double Iva { get => iva; set => iva = value; }
        public double Total { get => total; set => total = value; }
        public int Id_codigo { get => id_codigo; set => id_codigo = value; }
        public int Id_producto { get => id_producto; set => id_producto = value; }
        public int Cantida { get => cantida; set => cantida = value; }
        public double Precio1 { get => Precio; set => Precio = value; }
        public string Nombre_producto { get => nombre_producto; set => nombre_producto = value; }
        public string Nombre_cliente { get => nombre_cliente; set => nombre_cliente = value; }
        public string Apellido_cliente { get => apellido_cliente; set => apellido_cliente = value; }
    }
}
