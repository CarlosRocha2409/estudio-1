﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Estudio_2
{
    public partial class FrmCliente : Form
    {
        DataSet dsCliente;
        BindingSource bsCliente;
        DataTable tblCliente;
        DataRow drCliente;
        public FrmCliente()
        {
            InitializeComponent();
            bsCliente = new BindingSource();
        }

        public DataSet DsCliente { get => dsCliente; set => dsCliente = value; }
        public DataTable TblCliente { get => tblCliente; set => tblCliente = value; }
        public DataRow DrCliente {
            set
            {
                drCliente = value;
                msktxtCedula.Text= drCliente["Cedula"].ToString();
                txtNombre.Text = drCliente["Nombre"].ToString();
                txtApellido.Text= drCliente["Apellido"].ToString();
                msktxtCelular.Text = drCliente["Celular"].ToString();
                txtDireccion.Text = drCliente["Municipio"].ToString();
                txtDepartamento.Text = drCliente["Departamento"].ToString();

            }
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            String cedula, nombre, apellido, celular, direccion,municipio, departamento;
            cedula = msktxtCedula.Text;
            nombre = txtNombre.Text;
            apellido = txtApellido.Text;
            celular = msktxtCelular.Text;
            direccion = txtDireccion.Text;
            municipio = txtMunicipio.Text;
            departamento = txtDepartamento.Text;

            if(drCliente!= null) {
                DataRow drnew = tblCliente.NewRow();
                int index = tblCliente.Rows.IndexOf(drCliente);
                drnew["Id"] = drCliente["Id"];
                drnew["Nombre"] = nombre;
                drnew["Apellido"] = apellido;
                drnew["Celular"] = celular;
                drnew["Direccion"] = direccion;
                drnew["Municipio"] = municipio;
                drnew["Departamento"] = departamento;

                TblCliente.Rows.RemoveAt(index);
                TblCliente.Rows.InsertAt(drnew, index);
                TblCliente.Rows[index].AcceptChanges();
                TblCliente.Rows[index].SetModified();


            }
            else
            {
                TblCliente.Rows.Add(TblCliente.Rows.Count + 1, cedula, nombre, apellido, celular, direccion, municipio, departamento,nombre+" "+apellido);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
