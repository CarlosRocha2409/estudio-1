﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Estudio_2
{
    public partial class GestionCliente : Form
    {
        DataSet dsCliente;
        BindingSource bsCliente;

        public GestionCliente()
        {
            InitializeComponent();
            bsCliente = new BindingSource();

        }

        public DataSet DsCliente { get => dsCliente; set => dsCliente = value; }

        private void GestionCliente_Load(object sender, EventArgs e)
        {
            bsCliente.DataSource = DsCliente;
            bsCliente.DataMember = DsCliente.Tables["Cliente"].TableName;
            dataGridView1.DataSource = bsCliente;
            dataGridView1.AutoGenerateColumns = true;
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            FrmCliente fc = new FrmCliente();
            fc.DsCliente = DsCliente;
            fc.TblCliente = DsCliente.Tables["Cliente"];
            fc.ShowDialog();

        }

        private void BtnEditar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dataGridView1.SelectedRows;
            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder editar", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow row = ((DataRowView)gridRow.DataBoundItem).Row;

            FrmCliente fc = new FrmCliente();
            fc.DsCliente = DsCliente;
            fc.TblCliente = DsCliente.Tables["Cliente"];
            fc.DrCliente = row;
            fc.ShowDialog();

        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dataGridView1.SelectedRows;
            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder editar", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }

            DataGridViewRow gridRow = rowCollection[0];
            DataRow row = ((DataRowView)gridRow.DataBoundItem).Row;

            DialogResult result = MessageBox.Show(this, "Realmente Quiere Eliminar Esta Column?", "Mensaje De Error", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if(result== DialogResult.Yes)
            {
                DsCliente.Tables["Cliente"].Rows.Remove(row);
            }
        }

        private void TxtFinder_TextChanged(object sender, EventArgs e)
        {
            bsCliente.Filter = string.Format("Cedula like '*{0}*' or Nombre like '*{0}*' or Apellido like '*{0}*' or Celulular like '*{0}*' ",txtFinder);
        }
    }
}
