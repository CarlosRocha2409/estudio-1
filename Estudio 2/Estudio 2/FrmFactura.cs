﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Estudio_2
{
    public partial class FrmFactura : Form
    {
        DataSet dsFactura;
        BindingSource bsFactura;
        DataTable tblFactura;

        Double Subtotal,iva,total;
        int idFactura;

        public DataSet DsFactura { get => dsFactura; set => dsFactura = value; }
        public DataTable TblFactura { get => tblFactura; set => tblFactura = value; }

        public FrmFactura()
        {
            InitializeComponent();
            bsFactura = new BindingSource();
        }

        private void FrmFactura_Load(object sender, EventArgs e)
        {
            cmbCliente.DataSource = dsFactura.Tables["Cliente"];
            cmbCliente.DisplayMember = "NA";
            cmbCliente.ValueMember = "Id";

            cmbExtintores.DataSource = dsFactura.Tables["Extintor"];
            cmbExtintores.DisplayMember = "TM";
            cmbExtintores.ValueMember = "Id";

            dsFactura.Tables["ExtintorFactura"].Rows.Clear();
            bsFactura.DataSource = DsFactura.Tables["ExtintorFactura"];
            dataGridView1.DataSource = bsFactura;

            idFactura = dsFactura.Tables["Factura"].Rows.Count + 1;
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow drProducto = ((DataRowView)cmbExtintores.SelectedItem).Row;
                DataRow drProductoFactura = dsFactura.Tables["ExtintorFactura"].NewRow();
                drProductoFactura["Id"] = drProducto["Id"];
                //tipo marca medida capacidad lugar precio
                drProductoFactura["Tipo"] = drProducto["Tipo"];
                drProductoFactura["Marca"] = drProducto["Marca"];
                drProductoFactura["Cantidad"] = 1;
                drProductoFactura["Precio"] = drProducto["Precio"];
                dsFactura.Tables["ExtintorFactura"].Rows.Add(drProductoFactura);
            }
            catch (ConstraintException)
            {
                MessageBox.Show(this, "ERROR, producto ya agregado, verifique por favor!",
                    "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            CalcularFactura();
        }

        public void CalcularFactura()
        {
            Subtotal = 0;
            foreach (DataRow dr in dsFactura.Tables["ExtintorFactura"].Rows)
            {
                Subtotal += Int32.Parse(dr["Cantidad"].ToString()) * Double.Parse(dr["Precio"].ToString());
            }

            iva = Subtotal * 0.15;
            total = Subtotal + iva;

            txtSubtotal.Text = Subtotal.ToString();
            txtIva.Text = iva.ToString();
            txtTotal.Text = total.ToString();
        }

        private void DataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow gridRow = dataGridView1.Rows[e.RowIndex];
            DataRow RowFactura = ((DataRowView)gridRow.DataBoundItem).Row;
            DataRow drProductoFactura = dsFactura.Tables["ExtintorFactura"].Rows.Find(RowFactura["Id"]);

            if (Int32.Parse(RowFactura["Cantidad"].ToString()) > Int32.Parse(drProductoFactura["Cantidad"].ToString()))
            {
                MessageBox.Show(this,
                    "ERROR, la cantidad de producto a vender no puede ser mayor que la del almacen",
                    "Mensaje de error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                drProductoFactura["Cantidad"] = Int32.Parse(RowFactura["Cantidad"].ToString());

            }
            CalcularFactura();
        }

        private void DataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            CalcularFactura();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection RowCollection = dataGridView1.SelectedRows;
            if (RowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, no hay filas para eliminar!!", "Mensaje de error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataRow drProducto = ((DataRowView)RowCollection[0].DataBoundItem).Row;
            dsFactura.Tables["ExtintorFactura"].Rows.Remove(drProducto);
            CalcularFactura();
        }

        private void BtnFactura_Click(object sender, EventArgs e)
        {
            DataRow drFactura = dsFactura.Tables["Factura"].NewRow();

            drFactura["Id"] = idFactura;
            
            drFactura["Cliente"] = cmbCliente.SelectedValue;
            drFactura["Subtotal"] = txtSubtotal.Text;
            drFactura["Iva"] = txtIva.Text;
            drFactura["Total"] = txtTotal.Text;
            drFactura["Fecha"] = DateTime.Now;

            foreach(DataRow dr in DsFactura.Tables["ExtintorFactura"].Rows)
            {
                DataRow drDetalleFactura = dsFactura.Tables["DetalleFactura"].NewRow();

                drDetalleFactura["Id"] = drFactura["Id"];
                drDetalleFactura["Extintor"] = dr["Id"];
                drDetalleFactura["Cantidad"] = dr["Cantidad"];
                drDetalleFactura["Precio"] = dr["Precio"];
                dsFactura.Tables["DetalleFactura"].Rows.Add(drDetalleFactura);
                int detalleindex = dsFactura.Tables["DetalleFactura"].Rows.Count - 1;
                dsFactura.Tables["DetalleFactura"].Rows[detalleindex].AcceptChanges();
                dsFactura.Tables["DetalleFactura"].Rows[detalleindex].SetAdded();

                /*DataRow drProducto = dsFactura.Tables["Extintor"].Rows.Find(dr["Id"]);
                drProducto["Cantidad"] = Double.Parse(drProducto["Cantidad"].ToString()) -
                    Double.Parse(dr["Cantidad"].ToString());*/
                
            }
            FrmReporteFactura frf = new FrmReporteFactura(idFactura);
            frf.DsSistema = DsFactura;
            frf.ShowDialog();
        }
    }
}
