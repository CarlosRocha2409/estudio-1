﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Estudio_2
{
    public partial class FrmReporteFactura : Form
    {
        DataSet dsSistema;
        int FacturaId;
        DataRow drReporte;

        
        public FrmReporteFactura(int FacturaId)
        {
            InitializeComponent();
            this.FacturaId = FacturaId;
        }

        public DataSet DsSistema { get => dsSistema; set => dsSistema = value; }
        public DataRow DrReporte { set => drReporte = value; }

        private void FrmReporteFactura_Load(object sender, EventArgs e)
        {
            dsSistema.Tables["ReporteFactura"].Rows.Clear();

            dsSistema.Tables["ReporteFactura"].Rows.Clear();

            DataTable dtFactura = dsSistema.Tables["Factura"];
            DataRow drFactura = dtFactura.Rows[this.FacturaId - 1];
           
            DataRow drCliente = dsSistema.Tables["Cliente"].Rows.Find(drFactura["Cliente"]);

            DataTable dtDetalleFactura = dsSistema.Tables["DetalleFactura"];

            DataRow[] drDetallesFacturas =
                dtDetalleFactura.Select(String.Format("Factura = {0}",
                drFactura["Id"]));

            foreach (DataRow dr in drDetallesFacturas)
            {
                DataRow drReporteFactura = dsSistema.Tables["ReporteFactura"].NewRow();
                DataRow drProducto = dsSistema.Tables["Producto"].Rows.Find(dr["Producto"]);
                drReporteFactura["Id"] = drFactura["Id"];
                drReporteFactura["Fecha"] = drFactura["Fecha"];
                drReporteFactura["Subtotal"] = drFactura["Subtotal"];
                drReporteFactura["Iva"] = drFactura["Iva"];
                drReporteFactura["Total"] = drFactura["Total"];
                drReporteFactura["Nombre_Producto"] = drProducto["Nombre"];
                drReporteFactura["Cantidad"] = dr["Cantidad"];
                drReporteFactura["Precio"] = drProducto["Precio"];
                drReporteFactura["Nombre_Cliente"] = drCliente["NA"];
                dsSistema.Tables["ReporteFactura"].Rows.Add(drReporteFactura);
            }

            this.reportViewer1.LocalReport.ReportEmbeddedResource = "Estudio_2.ReporteExtintor.rdlc";
            ReportDataSource rds1 = new ReportDataSource("dsReporteFactura", dsSistema.Tables["ReporteFactura"]);
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(rds1);


            this.reportViewer1.RefreshReport();

        }
    }
}
